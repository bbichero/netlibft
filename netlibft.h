#ifndef NETLIBFT_H
# define NETLIBFT_H

# include <netdb.h>
# include <sys/socket.h>
# include <netinet/ip_icmp.h>
# include <arpa/inet.h>
# include <stdint.h>
# include <sys/types.h>

uint32_t  	ft_addrinfo_to_ip(struct addrinfo *addr);
unsigned char 	*ft_addrinfo_to_ipv6(struct addrinfo *addr6);

#endif
