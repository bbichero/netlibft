NAME = netlibft.a

OBJPATH = obj
SRCPATH = .

CC = gcc
BASEFLAGS = -Wall -Wextra
CFLAGS = $(BASEFLAGS) -Werror -O2 -g

RM = rm -rf

SRCSFILES = ft_ip.c
SRC = $(addprefix $(SRCPATH)/,$(SRCSFILES))
OBJECTS = $(SRC:$(SRCPATH)/%.c=$(OBJPATH)/%.o)

# Yellow
Y = \033[0;33m
# Red
R = \033[0;31m
# Green
G = \033[0;32m
# No color
E = \033[39m

all: $(NAME)

$(NAME): $(OBJECTS)
	@echo "$(Y)[NETLIBFT]$(E)"
	@echo "$(G)[COMPILING]$(E)"
	@echo "$(G)$(CC) $(CFLAGS) -c $(SRC) $(E)"
	@$(CC) $(CFLAGS) -c $(SRC)
	@mv *.o $(OBJPATH)
	@ar rc $(NAME) $(OBJECTS)
	@ranlib $(NAME)
	@echo "$(G)[COMPILING DONE]$(E)"

$(OBJECTS): $(OBJPATH)/%.o : $(SRCPATH)/%.c
	@echo "$(Y)[CREATE OBJ DIR]$(E)"
	@mkdir -p $(dir $@)
	@$(CC) $(CFLAGS) -c $<

clean:
	@$(RM) $(OBJPATH)
	@echo "$(Y)[NETLIBFT]$(E)"
	@echo "$(R)[ALL .O DELETE]$(E)"

fclean: clean
	@$(RM) $(NAME)
	@echo "$(R)[NETLIBFT.A DELETE]$(E)"
re: fclean all
