#include "netlibft.h"

uint32_t		ft_addrinfo_to_ip(struct addrinfo *addr)
{
	return (((struct sockaddr_in *)addr->ai_addr)->sin_addr.s_addr);
}

unsigned char		*ft_addrinfo_to_ipv6(struct addrinfo *addr6)
{
	return (((struct sockaddr_in6 *)addr6->ai_addr)->sin6_addr.s6_addr);
}
